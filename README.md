# In Store Pickup

Service provides the APIs for all In store Pickup details
- stores
- store_timings
- store_slots
- store_timings

### API Details:


### Pre-requisites to run the app:

1 - Setup and Run Postgres DB locally.

2 - Create `USER`, `ROLE` & `DATABASE` on Postgres:
  
`$ psql postgres`<br />
`postgres=# CREATE USER store_pickup WITH PASSWORD '<YOUR-USER-PASSWORD>';`<br />
`postgres=# CREATE ROLE store_pickup WITH CREATEDB CREATEROLE;`<br />
`postgres=# CREATE DATABASE store_pickup;`

3 - Grant `PRIVILEGES` on Database TO User:

`postgres=# GRANT CREATE ON DATABASE store_pickup TO store_pickup;`


### Environment Variables:

| Variable | Value                                |
|----------------------|--------------------------------------|
| DB_USER              | DB User                              |
| DB_HOST              | DB Host                              |
| DB_NAME              | DB Database                          |
| DB_PASSWORD          | DB Password                          |
| DB_PORT              | DB Port                              |
| NODE_ENV             | development / staging / production.  |
| GOOGLE_API_KEY       | Google API Key values                |


