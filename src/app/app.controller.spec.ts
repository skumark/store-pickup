import { Test, TestingModule } from '@nestjs/testing';
import { AppController, LiveResponse } from './app.controller';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
    }).compile();

    appController = module.get<AppController>(AppController);
  });

  it('should be defined', () => {
    expect(appController).toBeDefined();
  });

  it('should return live status response', () => {
    const liveResponse = appController.live();
    expect(liveResponse).toEqual(new LiveResponse('UP'));
  });
});
