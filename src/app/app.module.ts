import { Module } from '@nestjs/common';

import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_FILTER } from '@nestjs/core';
import { AllExceptionsFilter } from '../common/exceptions/allExceptions.filter';
import { AppController } from './app.controller';
import { StoreModule } from '../api/store/store.module';
import { PrometheusModule } from '@willsoto/nestjs-prometheus';

@Module({
  imports: [
    ConfigModule.forRoot(),
    StoreModule,
    PrometheusModule.register({
      path: '/prometheus',
    }),
  ],
  providers: [
    AppController,
    ConfigService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  controllers: [AppController],
})
export class AppModule {}
