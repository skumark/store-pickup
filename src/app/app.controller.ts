import { Controller, Get } from '@nestjs/common';

export class LiveResponse {
  public readonly status: string;

  constructor(status: string) {
    this.status = status;
  }
}

@Controller()
export class AppController {
  @Get('/live') live(): LiveResponse {
    return new LiveResponse('UP');
  }
}
