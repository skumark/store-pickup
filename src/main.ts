import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { AllExceptionsFilter } from './common/exceptions/allExceptions.filter';
import { NestFastifyApplication } from '@nestjs/platform-fastify';

const appPort = process.env.PORT || 3004;

import './tracer';
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
async function bootstrap() {
  try {
    const app = await NestFactory.create<NestFastifyApplication>(AppModule);
    const { httpAdapter } = app.get(HttpAdapterHost);
    app.useGlobalFilters(new AllExceptionsFilter(httpAdapter));

    app.enableCors();
    await app.listen(appPort);
  } catch (error) {
    console.error(error);
  }
}

bootstrap();
