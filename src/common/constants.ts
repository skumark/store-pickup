export const statusCode = {
  OK: 200,
  BAD_REQUEST: 400,
  INTERNAL_SERVER_ERROR: 500,
  SUCCESS: 201,
  NOT_FOUND: 404,
};

export const statusMessage = {
  BAD_REQUEST: 'Bad Request',
  BAD_REQUEST_INVALID_ID: 'Specified value for seller center data is invalid',
  BAD_REQUEST_INVALID_ID_ARRAY: "Specified value for 'seller center id' array is invalid",
  BAD_REQUEST_INVALID_TENANT: "Specified value for 'tenant' is invalid",
  BAD_REQUEST_INVALID_OPERATOR_CODE: "Specified value for 'operatorCode' is invalid",
  BAD_REQUEST_INVALID_ATTRIBUTE_GROUP: "Specified value for 'buAttributeGroup' is invalid",
  BAD_REQUEST_INVALID_PARAMETERS: 'Empty parameters, check the swagger to see the correct parameters',
  BAD_REQUEST_INVALID_SELLER_CHANNEL: "Specified value for 'sellerChannel' is invalid",
  BAD_REQUEST_MISSING_PARAMETERS: 'Missing parameters, check the swagger to see the correct parameters',
  NOT_FOUND_PLATFORM_MAPPING: 'Mappings not found for given sellerProductId/sellerSkuId',
};

export const storeTable = {
  storeId: 'store_id',
  storeName: 'store_name',
  addressLine1: 'address_line1',
  addressLine2: 'address_line2',
  country: 'country',
  province: 'province',
  region: 'region',
  postcode: 'postcode',
  latitude: 'latitude',
  longitude: 'longitude',

};

export const weekday = {
  0: "Sunday",
  1: "Monday",
  2: "Tuesday",
  3: "Wednesday",
  4: "Thursday",
  5: "Friday",
  6: "Saturday"
};

export const storeTimingTable = {
  storeId: 'store_id',
  dayOfWeek: 'day_of_week',
  startTime: 'start_time',
  endTime: 'end_time',
  slotInterval: 'slot_interval',
  orderPerSlot: 'order_per_slot',
  collectTime: 'collect_time'
};

export const storeSlotTable = {
  storeId: 'store_id',
  slotStartTime: 'slot_start_time',
  slotEndTime: 'slot_end_time',
  storeTimingId: 'store_timing_id',
};

export const storeOrderTable = {
  orderId: 'order_id',
  userId: 'user_id',
  storeSlotId: 'store_slot_id',
  orderTime: 'order_time',
  pickupDate: 'pickup_date',
};

// todo -> the name should not be http response
export const httpResponse = {
  OK: 'Ok',
  SUCCESS: 'success',
  ERROR: 'error',
  DATABASE_ERROR: 'Database Error',
  NOT_FOUND: 'Data not found',
};
