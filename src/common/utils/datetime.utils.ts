export class DatetimeUtils {
  static getUtcTimestampInSec(): number {
    const dateObj = new Date();
    const utcTimestampInMilliSec = dateObj.getTime();
    return Math.floor(utcTimestampInMilliSec / 1000);
  }

  static calculateExpiryTimeInSec(timestamp, expireAfterMinutes): number {
    const expireAfterSecs = expireAfterMinutes * 60;
    return timestamp + expireAfterSecs;
  }
}
