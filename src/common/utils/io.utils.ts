import * as winston from 'winston';

const { combine, timestamp, json } = winston.format;

const log = (labelInfo, message, isSilent, details): void => {
  const consoleTransport = new winston.transports.Console();
  const loggerConfig = {
    format: combine(timestamp(), json()),
    transports: [consoleTransport],
    silent: isSilent,
  };
  const logger = winston.createLogger(loggerConfig);
  labelInfo ? logger.info(message, details) : logger.error(message, details);
};

export const logInfo = (message, details): void => {
  log(true, message, false, details);
};

export const logError = (message, details): void => {
  log(false, message, false, details);
};

export const logRequest = (message, request): void => {
  logInfo(message, request);
};

export const logRequestAndResponse = (message, request, status, response): void => {
  logInfo(message, { request: request, status: status, response: response });
};
