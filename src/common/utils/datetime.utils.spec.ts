import * as mockdate from 'mockdate';
import { DatetimeUtils } from './datetime.utils';

describe('datetime utils', () => {
  beforeEach(() => {
    mockdate.set('2020-12-22T09:14:07.637Z');
  });

  it('should get current timestamp in utc in seconds', () => {
    const timestamp = DatetimeUtils.getUtcTimestampInSec();
    expect(timestamp).toBe(1608628447);
  });

  it('should return expiry time in seconds', function() {
    const timestamp = 1608628447;
    const expireAt = DatetimeUtils.calculateExpiryTimeInSec(timestamp, 20);
    expect(expireAt).toBe(1608629647);
  });

  it('should return same timestamp if expiry time is 0', function() {
    const timestamp = 1608628447;
    const expireAt = DatetimeUtils.calculateExpiryTimeInSec(timestamp, 0);
    expect(expireAt).toBe(timestamp);
  });
});
