import { difference, has, isEmpty, uniq } from 'lodash';
import { DataNotFoundError, FileNotFoundError } from '../exceptions/exceptions';

export const validateApiResult = (apiInfo): any => {
  if (!isValueInObject(apiInfo, 'result')) return false;
  const response = apiInfo.result.filter(value => value.status === 'success');
  return response.length === apiInfo.result.length;
};

// todo -> should be moved to datetime utils
export const getCurrentDateTime = (): string => {
  return new Date().toISOString();
};

export const toArray = (value): any => (Array.isArray(value) ? value : [value]);

// todo -> should be === instead of ==.
export const isNull = (value): boolean => value == null;

export const isStringInvalid = (id): boolean => !id || isNull(id) || id.trim() === '';

export const areStringsInvalid = (ids: string[]): boolean => ids.some(id => isStringInvalid(id));

// isNullOrEmptyOrUndefined
export const isNullOrEmpty = (value): boolean => {
  return value == null || isEmpty(value);
};

export const isArrayInvalid = (arr): boolean => {
  return !arr || isNullOrEmpty(arr);
};

export const isValueInObject = (object, key): any => has(object, key);

export const validateField = (object, key, value): boolean => has(object, key) && !isNullOrEmpty(value);

export const hasNonNumericInvalidField = (object: any, key: string): boolean => {
  if (!has(object, key)) {
    return true;
  }
  const value = object[key];
  return isNullOrEmpty(value);
};

export const hasAnyNonNumericInvalidField = (object: any, keys: string[]): boolean => {
  return keys.some(key => hasNonNumericInvalidField(object, key));
};

export const isValueValid = (value, object): boolean => Object.values(object).includes(value);

export const deleteDuplicate = (arr: any[]): any[] => uniq(arr);
