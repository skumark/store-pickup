import {
  areStringsInvalid,
  deleteDuplicate,
  hasAnyNonNumericInvalidField,
  isArrayInvalid,
  isNull,
  isNullOrEmpty,
  isStringInvalid,
  isValueInObject,
  isValueValid,
  toArray,
  validateApiResult,
  validateField,
} from './helper';
import { DataNotFoundError, FileNotFoundError } from '../exceptions/exceptions';


describe('helper', () => {

  afterEach(() => {
    jest.resetAllMocks();
    jest.clearAllMocks();
  });

  it('should validate Api Result and return true', () => {
    const value = {
      result: [
        { status: 'success', product: {} },
        { status: 'success', product: {} },
      ],
    };
    const response = validateApiResult(value);
    expect(response).toBe(true);
  });

  it('should validate Api Result and return false', () => {
    const value = {
      result: [
        { status: 'failure', product: {} },
        { status: 'success', product: {} },
      ],
    };
    const response = validateApiResult(value);
    expect(response).toBe(false);
  });

  it('should convert to array when it is not array', () => {
    const result = toArray('id');
    expect(result).toMatchObject(['id']);
  });

  it('should keep as array if input is array', () => {
    const result = toArray(['id']);
    expect(result).toMatchObject(['id']);
  });

  it('should return true when param is null', () => {
    const result = isNull(null);

    expect(result).toBe(true);
  });

  it('should return false when param is not null', () => {
    const result = isNull(1);

    expect(result).toBe(false);
  });

  it('should return true if string is null', () => {
    const result = isStringInvalid(null);
    expect(result).toBe(true);
  });

  it('should return true if string is empty', () => {
    const result = isStringInvalid('');
    expect(result).toBe(true);
  });

  it('should return false if string is valid', () => {
    const result = isStringInvalid('valid-string');
    expect(result).toBe(false);
  });

  it('should return true when param is null', () => {
    const result = isNullOrEmpty(null);
    expect(result).toBe(true);
  });

  it('should return true when param is empty', () => {
    const result = isNullOrEmpty('');
    expect(result).toBe(true);
  });

  it('should return false when param is a proper string', () => {
    const result = isNullOrEmpty('some-string');
    expect(result).toBe(false);
  });

  it('should return true if array is null', () => {
    const result = isArrayInvalid(null);
    expect(result).toBe(true);
  });

  it('should return true if array is empty', () => {
    const result = isArrayInvalid([]);
    expect(result).toBe(true);
  });

  it('should return false if array has atleast one entry', () => {
    const result = isArrayInvalid(['']);
    expect(result).toBe(false);
  });

  it('should return true if the key is in the object', () => {
    const result = isValueInObject({ data: 'data' }, 'data');

    expect(result).toBe(true);
  });

  it('should return false if the key is not in the object', () => {
    const result = isValueInObject({ data: 'data' }, 'id');

    expect(result).toBe(false);
  });

  it('should return true if the field is not null', () => {
    const object = { data: 'data' };
    const result = validateField(object, 'data', object.data);

    expect(result).toBe(true);
  });

  it('should return false if the field is null', () => {
    const object = { data: 'data', id: null };
    const result = validateField(object, 'id', object.id);

    expect(result).toBe(false);
  });

  it('should return false if the value is not a valid value', () => {
    const object = { data: 'data' };
    const result = isValueValid('id', object);

    expect(result).toBe(false);
  });

  it('should return true if the value is a valid value', () => {
    const object = { data: 'data' };
    const result = isValueValid('data', object);

    expect(result).toBe(true);
  });

  it('should return array with no duplicates', () => {
    const result = deleteDuplicate(['id01', 'id01']);

    expect(result).toMatchObject(['id01']);
  });

  it('should return true if a field is not present', function() {
    const result = hasAnyNonNumericInvalidField({ a: 'apple', b: 'ball' }, ['a', 'c']);
    expect(result).toBe(true);
  });

  it('should return true if a field is null', function() {
    const result = hasAnyNonNumericInvalidField({ a: null, b: 'ball' }, ['a', 'b']);
    expect(result).toBe(true);
  });

  it('should return true if field is empty string', function() {
    const result = hasAnyNonNumericInvalidField({ a: '', b: 'ball' }, ['a', 'b']);
    expect(result).toBe(true);
  });

  it('should return false if all the fields are valid', function() {
    const result = hasAnyNonNumericInvalidField({ a: 'apple', b: 'ball' }, ['a', 'b']);
    expect(result).toBe(false);
  });

  it('should return true if one of the strings are invalid', function() {
    const result = areStringsInvalid(['', 'valid1', 'valid2']);
    expect(result).toBe(true);
  });

  it('should return false if all the strings are valid', function() {
    const result = areStringsInvalid(['valid0', 'valid1', 'valid2']);
    expect(result).toBe(false);
  });
});
