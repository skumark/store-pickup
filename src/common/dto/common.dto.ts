export interface AuthHeader {
  readonly Authorization: string;
}
