import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class EnvConfigService {
 
  readonly NODE_ENV;
  readonly GOOGLE_API_KEY;

  constructor(private readonly configService: ConfigService) {
    this.GOOGLE_API_KEY = this.configService.get('GOOGLE_API_KEY');
    this.NODE_ENV = this.configService.get('NODE_ENV');
  }
}
