import axios from 'axios';

const request = axios.create();
request.defaults.timeout = 30000;

export { request };
