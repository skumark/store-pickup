import {
  hasAnyNonNumericInvalidField,
  isArrayInvalid,
  isNullOrEmpty,
  isStringInvalid,
  validateField,
} from '../../utils/helper';
import { BadRequestError } from '../../exceptions/exceptions';
import { statusMessage } from '../../constants';
export class RequestValidator {
  static validateRequest(id): void {
    if (isStringInvalid(id)) {
      throw new BadRequestError(statusMessage.BAD_REQUEST_INVALID_ID);
    }
  }
  static validateStorePostRequest(storeId, storeName, addressLine1, latitude, longitude): void {
    if (isStringInvalid(storeId) && isStringInvalid(storeName) && isStringInvalid(addressLine1) && isStringInvalid(latitude) && isStringInvalid(longitude)) {
      throw new BadRequestError(statusMessage.BAD_REQUEST_INVALID_ID);
    }
  }
}
