const { DB_USER, DB_HOST, DB_NAME, DB_PASSWORD, DB_PORT } = process.env;

const dbConfig = {
  user: DB_USER,
  host: DB_HOST,
  database: DB_NAME,
  password: DB_PASSWORD,
  port: DB_PORT,
};

const dbSchema = 'store_pickup';
const JWT_ALGORITHM = 'RS256';
const EXPIRE_TOKEN_AFTER_MINUTES = 43800;

export { dbConfig, dbSchema, EXPIRE_TOKEN_AFTER_MINUTES, JWT_ALGORITHM };
