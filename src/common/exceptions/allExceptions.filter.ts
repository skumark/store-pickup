import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { BadRequestError, DatabaseError, DataNotPresentInDB, HydraApiError } from './exceptions';

@Catch()
export class AllExceptionsFilter<T> implements ExceptionFilter {
  private httpAdapter: HttpAdapterHost;

  constructor(httpAdapter: HttpAdapterHost) {
    this.httpAdapter = httpAdapter;
  }

  catch(exception: T, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    Logger.error('Exception occurred while processing request: ' + exception);

    switch (exception.constructor) {
      case DatabaseError:
      case BadRequestError:
        this.sendResponse(exception, response, request);
        return;
      case HydraApiError:
      case DataNotPresentInDB:
        this.sendResponseWithMsg(exception, response, request);
        return;

      default:
        this.sendResponseDefault(exception, response, request);
    }
  }

  private sendResponse(exception, response, request): void {
    const status = exception.responseStatusCode;
    response.status(status).json({ statusCode: status, path: request.url });
  }

  private sendResponseWithMsg(exception, response, request): void {
    const status = exception.responseStatusCode;
    const payload = { statusCode: status, path: request.url, message: exception.message };
    response.status(status).json(payload);
  }

  private sendResponseDefault(exception, response, request): void {
    const status = exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;
    response.status(status).json({ statusCode: status, path: request.url });
  }
}
