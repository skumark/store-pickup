import { httpResponse, statusCode } from '../constants';

export class BadRequestError extends Error {
  public readonly responseStatusCode: number = statusCode.BAD_REQUEST;
  // todo -> move validation error to response object (httpResponse)
  public readonly type: string = 'Validation Error';

  constructor(message: string) {
    super(message);
  }
}

export class DatabaseError extends Error {
  public readonly responseStatusCode: number = statusCode.INTERNAL_SERVER_ERROR;
  public readonly type: string = httpResponse.DATABASE_ERROR;

  constructor(message: string) {
    super(message);
  }
}

export class FileNotFoundError extends Error {
  public readonly responseStatusCode: number = statusCode.INTERNAL_SERVER_ERROR;
  public readonly type: string = httpResponse.NOT_FOUND;

  constructor(filePath: string) {
    super('File Not Found: ' + filePath);
  }
}

export class DataNotFoundError extends Error {
  public readonly responseStatusCode: number = statusCode.INTERNAL_SERVER_ERROR;
  public readonly type: string = httpResponse.NOT_FOUND;

  constructor(message: string) {
    super(message);
  }
}

export class DataNotPresentInDB extends Error {
  public readonly responseStatusCode: number = statusCode.NOT_FOUND;
  public readonly type: string = httpResponse.NOT_FOUND;

  constructor(message: string) {
    super(message);
  }
}

export class HydraApiError extends Error {
  public readonly responseStatusCode: number = statusCode.INTERNAL_SERVER_ERROR;
  public readonly type: string = 'Hydra Api Error';

  constructor(message: string) {
    super(`Hydra Api: ${message}`);
  }
}
