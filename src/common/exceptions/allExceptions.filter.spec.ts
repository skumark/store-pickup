import { AllExceptionsFilter } from './allExceptions.filter';
import { Test, TestingModule } from '@nestjs/testing';
import { HttpAdapterHost } from '@nestjs/core';
import { HttpException, HttpStatus } from '@nestjs/common';
import { ExecutionContextHost } from '@nestjs/core/helpers/execution-context-host';
import { DatabaseError, DataNotPresentInDB, HydraApiError } from './exceptions';

describe('AllExceptionsFilter', () => {
  let allExceptionsFilter: AllExceptionsFilter<any>;

  const mockJson = jest.fn();
  const mockStatus = jest.fn().mockImplementation(() => ({
    json: mockJson,
  }));
  const mockGetResponse = jest.fn().mockImplementation(() => ({
    status: mockStatus,
  }));

  const mockGetRequest = jest.fn().mockImplementation(() => ({
    url: 'some-url',
  }));
  const mockHttpArgumentsHost = jest.fn().mockImplementation(() => ({
    getResponse: mockGetResponse,
    getRequest: mockGetRequest,
  }));

  const mockArgumentsHost = {
    switchToHttp: mockHttpArgumentsHost,
    getArgByIndex: jest.fn(),
    getArgs: jest.fn(),
    getType: jest.fn(),
    switchToRpc: jest.fn(),
    switchToWs: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AllExceptionsFilter, ExecutionContextHost, HttpAdapterHost],
    }).compile();

    allExceptionsFilter = module.get<AllExceptionsFilter<any>>(AllExceptionsFilter);
  });

  it('should be defined', () => {
    expect(allExceptionsFilter).toBeDefined();
  });

  it('should return response for Hydra api error', function() {
    const exceptionMessage = 'Response failed with status code 409';
    allExceptionsFilter.catch(new HydraApiError(exceptionMessage), mockArgumentsHost);

    const expectedPayload = {
      statusCode: 500,
      path: 'some-url',
      message: `Hydra Api: Response failed with status code 409`,
    };
    expect(mockStatus).toBeCalledTimes(1);
    expect(mockStatus).toBeCalledWith(500);
    expect(mockJson).toBeCalledTimes(1);
    expect(mockJson).toBeCalledWith(expectedPayload);
  });

  it('should return response for httpException', () => {
    allExceptionsFilter.catch(new HttpException('Http exception', HttpStatus.BAD_REQUEST), mockArgumentsHost);
    expect(mockHttpArgumentsHost).toBeCalledTimes(1);
    expect(mockHttpArgumentsHost).toBeCalledWith();
    expect(mockGetResponse).toBeCalledTimes(1);
    expect(mockGetRequest).toBeCalledTimes(1);
    expect(mockStatus).toBeCalledTimes(1);
    expect(mockStatus).toBeCalledWith(HttpStatus.BAD_REQUEST);
    expect(mockJson).toBeCalledTimes(1);
    expect(mockJson).toBeCalledWith({
      statusCode: 400,
      path: 'some-url',
    });
  });

  it('should return response for non httpException', () => {
    allExceptionsFilter.catch(new Error(), mockArgumentsHost);

    expect(mockJson).toBeCalledWith({
      statusCode: 500,
      path: 'some-url',
    });
  });

  it('should return response for DataNotPresentInDB exception', () => {
    const message = 'Data not present';
    allExceptionsFilter.catch(new DataNotPresentInDB(message), mockArgumentsHost);
    expect(mockJson).toBeCalledWith({ statusCode: 404, path: 'some-url', message });
  });

  it('should return response for Batabase exception', () => {
    const message = 'Error connecting db';
    allExceptionsFilter.catch(new DatabaseError(message), mockArgumentsHost);
    expect(mockJson).toBeCalledWith({ statusCode: 500, path: 'some-url' });
  });
});
