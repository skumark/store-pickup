import { StoreController } from './store.controller';
import { StoreService } from './store.service';
import { Test, TestingModule } from '@nestjs/testing';
import { mockedstorePostRequest } from './__testData__/store.testData.spec';
import { beforeEach } from '@jest/globals';
import { RequestValidator } from '../../common/services/validation/validation.service';
import { BadRequestError } from '../../common/exceptions/exceptions';

jest.mock('./store.service');

describe('store controller', () => {
  let controller: StoreController;
  let service: StoreService;

  beforeEach(async () => {
    const metadata = {
      providers: [StoreService],
      controllers: [StoreController],
    };
    const module: TestingModule = await Test.createTestingModule(metadata).compile();

    service = module.get<StoreService>(StoreService);
    controller = module.get<StoreController>(StoreController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('POST store', () => {
    const postResponse: { message: string } = { message: 'success' };

    beforeEach(() => {
      jest.spyOn(service, 'postStore').mockReturnValue(Promise.resolve(postResponse));
    });

    it('should post store', async () => {
      jest.spyOn(service, 'postStore').mockReturnValueOnce(Promise.resolve(postResponse));

      const actual = await controller.postStore(mockedstorePostRequest);

      expect(actual).toBe(postResponse);
      expect(service.postStore).toHaveBeenCalledWith(mockedstorePostRequest);
    });
  });
});
