import { Body, Controller, Delete, Get, HttpCode, Param, Post, Query } from '@nestjs/common';
import { StoreService } from './store.service';
import { StoreGETResponse } from './dto/storeResponse';
import { GETAllStoresResponse } from './dto/storeResponse';
import { StorePostRequest } from './dto/storeRequests';
import { StoreGetAvailableSlots } from './dto/StoreAvailableSlots';
import { StoreTimingsPostRequest } from './dto/storeTimingsRequest';
import { StoreSlotsPostRequest } from './dto/storeSlotsRequest';
import { StoreOrdersPostRequest } from './dto/storeOrdersRequest';
import { logRequest, logRequestAndResponse } from '../../common/utils/io.utils';
import { RequestValidator } from '../../common/services/validation/validation.service';
import { httpResponse } from '../../common/constants';

@Controller('/api/v1/store')
export class StoreController {
  constructor(private readonly storeService: StoreService) {}

  @Post()
  async postStore(@Body() request: StorePostRequest): Promise<{ message: string }> {
    logRequest('Incoming store post request', request);
    // const { storeId, storeName, addressLine1, addressLine2, latitude, longitude } = request;
    const response = await this.storeService.postStore(request);

    logRequestAndResponse('Response details post store', request, httpResponse.SUCCESS, response);
    return response;
  }

  @Post('/timings')
  async postStoreTimings(@Body() request: StoreTimingsPostRequest): Promise<{ message: string }> {
    logRequest('Incoming store timings post request', request);
    // const { storeId, dayOfWeek, startTime, endTime, slotInterval, orderPerSlot, collectTime } = request;
    const response = await this.storeService.postStoreTimings(request);

    logRequestAndResponse('Response details post store timings' , request, httpResponse.SUCCESS, response);
    return response;
  }

  @Post('/slots')
  async postStoreSlots(@Body() request: StoreSlotsPostRequest): Promise<{ message: string }> {
    logRequest('Incoming store post request', request);
    // const { storeId, slotStartTime, slotEndTime, storeTimingId } = request;
    const response = await this.storeService.postStoreSlots(request);

    logRequestAndResponse('Response details post store slots' , request, httpResponse.SUCCESS, response);
    return response;
  }

  @Post('/orders')
  async postStoreOrders(@Body() request: StoreOrdersPostRequest): Promise<{ message: string }> {
    logRequest('Incoming store post request', request);
    // const { orderId, userId, storeSlotId, orderTime, pickupDate } = request;
    const response = await this.storeService.postStoreOrders(request);

    logRequestAndResponse('Response details post store slots' , request, httpResponse.SUCCESS, response);
    return response;
  }

  @Get('/all')
  async getStores(
    @Query('country') country: string,
    @Query('province') province: string
  ): Promise<GETAllStoresResponse> {
    logRequest('Incoming get all store request', { country, province });

    const response = await this.storeService.getAllStore(country, province);

    logRequestAndResponse(
      'Response details get store',
      { country, province },
      httpResponse.SUCCESS,
      response,
    );
    return response;
  }


  @Get('/:storeId')
  async getStoreDetails(
    @Param('storeId') storeId: string
  ): Promise<StoreGETResponse> {
    logRequest('Incoming get store request', { storeId });

    console.log(storeId)
    RequestValidator.validateRequest(storeId);
    const response = await this.storeService.getStore(storeId);

    logRequestAndResponse(
      'Response details get store',
      { storeId },
      httpResponse.SUCCESS,
      response,
    );
    return response;
  }

  @Get('/:storeId')
  async getStoreAvailability(
    @Param('storeId') storeId: string,
    @Query('date') date: string
  ): Promise<StoreGetAvailableSlots> {
    logRequest('Incoming get store available slots request', { storeId, date });

    RequestValidator.validateRequest(storeId);
    RequestValidator.validateRequest(date);
    const response = await this.storeService.getStoreAvailability(storeId, date);

    logRequestAndResponse(
      'Response details get store',
      { storeId, date },
      httpResponse.SUCCESS,
      response,
    );
    return response;
  }
}
