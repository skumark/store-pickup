import { StorePostRequest } from './dto/storeRequests';
import { StoreGETResponse } from './dto/storeResponse';
import { GETAllStoresResponse } from './dto/storeResponse';
import { StoreGetAvailableSlots } from './dto/StoreAvailableSlots';
import { StoreTimingsPostRequest } from './dto/storeTimingsRequest';
import { StoreSlotsPostRequest } from './dto/storeSlotsRequest';
import { StoreOrdersPostRequest } from './dto/storeOrdersRequest';
import { httpResponse } from '../../common/constants';
import { Injectable } from '@nestjs/common';
import { StoreRepository } from '../../database/storeRepository/store.repository';
import { DataNotPresentInDB } from '../../common/exceptions/exceptions';
import { EnvConfigService } from '../../common/services/config/envConfig.service';
import { NodeGeocoder } from 'node-geocoder';
import { ConfigService } from '@nestjs/config';
import { isEmpty } from 'lodash';

@Injectable()
export class StoreService {
  constructor(
    private readonly storeRepository: StoreRepository,
    private readonly envConfigService: EnvConfigService,
  ) {}

  async postStore(request: StorePostRequest): Promise<{ message: string }> {
    const { storeId, storeName, addressLine1, addressLine2, country, province, region, postcode, latitude, longitude } = request;

    // const response = await this.getNearestStoreByLoc(latitude, longitude);
    // console.log(response)

    await this.storeRepository.createStore(storeId, storeName, addressLine1, addressLine2, country, province, region, postcode, latitude, longitude);

    return { message: httpResponse.SUCCESS };
  }

  async postStoreTimings(request: StoreTimingsPostRequest): Promise<{ message: string }> {
    for (let i = 0; i < 7; i++) {
      const { storeId, dayOfWeek, startTime, endTime, slotInterval, orderPerSlot, collectTime } = request[i];

      this.storeRepository.createTimings(storeId, dayOfWeek, startTime, endTime, slotInterval, orderPerSlot, collectTime);
    }
    return { message: httpResponse.SUCCESS };
  }

  async postStoreSlots(request: StoreSlotsPostRequest): Promise<{ message: string }> {
    const { storeId, slotStartTime, slotEndTime, storeTimingId } = request;

    await this.storeRepository.createSlots(storeId, slotStartTime, slotEndTime, storeTimingId);

    return { message: httpResponse.SUCCESS };
  }

  async postStoreOrders(request: StoreOrdersPostRequest): Promise<{ message: string }> {
    const { orderId, userId, storeSlotId, orderTime, pickupDate } = request;

    await this.storeRepository.createOrders(orderId, userId, storeSlotId, orderTime, pickupDate);

    return { message: httpResponse.SUCCESS };
  }

  async getStore(storeId: string): Promise<StoreGETResponse> {
    const result = await this.storeRepository.getStoreDetails(storeId);

    return this.createResponse(storeId, result);
  }

  async getAllStore(country: string, province: string): Promise<GETAllStoresResponse> {
    const result = await this.storeRepository.getAllStores(country, province);

    return this.allStoresResponse(result);
  }

  async getStoreAvailability(storeId: string, date: string): Promise<StoreGetAvailableSlots> {
    const result = await this.storeRepository.getStoreAvailability(storeId, date);

    return this.createAvailabilityResponse(storeId, date, result);
  }

  async getNearestStoreByLoc(lat: string, lon: string): Promise<any> {
    console.log('inside node geocoder')
    const gCloudNedeGeoCodeoptions = {
      provider: 'google',
      httpAdapter: 'https',
      apiKey: this.envConfigService.GOOGLE_API_KEY,
      formatter: null
    };
    console.log(gCloudNedeGeoCodeoptions);
    const geocoder = NodeGeocoder(gCloudNedeGeoCodeoptions);
    console.log(geocoder)
    const res = await geocoder.reverse({ lat, lon});
    return res;
  }

  private allStoresResponse(
    respArr: object,
  ): GETAllStoresResponse {
    if (isEmpty(respArr)) {
      throw new DataNotPresentInDB(`No stores found`);
    }

    return {results: respArr}
  }

  private createResponse(
    id: string,
    respArr: object,
  ): StoreGETResponse {
    if (isEmpty(respArr)) {
      throw new DataNotPresentInDB(`Data not present For storeId: ${id}`);
    }

    const { store_id, store_name, address_line1, address_line2, country, province, region, postcode, latitude, longitude } = respArr[0];

    return { storeId: store_id, storeName: store_name, addressLine1: address_line1, addressLine2: address_line2, country, province, region, postcode, latitude, longitude };
  }

  private createAvailabilityResponse(
    id: string,
    date: string,
    respArr: object,
  ): StoreGetAvailableSlots {

    return { storeId: '', storeName: '', date: '', dayofweek: '', slots: {}};
  }

}
