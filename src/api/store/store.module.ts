import { Module } from '@nestjs/common';
import { StoreService } from './store.service';
import { StoreController } from './store.controller';
import { EnvConfigModule } from '../../common/services/config/envConfig.module';
import { DatabaseModule } from '../../database/database.module';

@Module({
  imports: [DatabaseModule, EnvConfigModule],
  providers: [StoreService],
  controllers: [StoreController],
})
export class StoreModule {}
