import { StorePostRequest } from '../dto/storeRequests';

export const mockedstorePostRequest: StorePostRequest = {
  storeId: 'SOCL',
  storeName: 'GSC009',
  addressLine1: 'GSC Category Name-9',
  addressLine2: 'SODIMAC',
  latitude: '1323244',
  longitude: '9869557',
  country: 'CHILE',
  province: 'SANTIAGO',
  region: 'xyz',
  postcode: '12456'
};
