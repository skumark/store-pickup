export interface StoreTimingsPostRequest {
  readonly storeId: string;
  readonly dayOfWeek: string;
  readonly startTime: string;
  readonly endTime: string;
  readonly slotInterval: string;
  readonly orderPerSlot: string;
  readonly collectTime: string;
}
