export interface StoreGetAvailableSlots {
  readonly storeId: string;
  readonly storeName: string;
  readonly date: string;
  readonly dayofweek: string;
  readonly slots: object;
}
