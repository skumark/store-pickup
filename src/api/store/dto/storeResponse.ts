export interface StoreGETResponse {
  readonly storeId: string;
  readonly storeName: string;
  readonly addressLine1: string;
  readonly addressLine2: string;
  readonly country: string;
  readonly province: string;
  readonly region: string;
  readonly postcode: string;
  readonly latitude: string;
  readonly longitude: string;
}

export interface GETAllStoresResponse {
  readonly results: object
}