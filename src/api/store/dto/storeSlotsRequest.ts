export interface StoreSlotsPostRequest {
  readonly storeId: string;
  readonly slotStartTime: string;
  readonly slotEndTime: string;
  readonly storeTimingId: string;
}
