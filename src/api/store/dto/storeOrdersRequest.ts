export interface StoreOrdersPostRequest {
  readonly orderId: string;
  readonly userId: string;
  readonly storeSlotId: string;
  readonly orderTime: string;
  readonly pickupDate: string;
}
