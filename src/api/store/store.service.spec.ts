import { RequestValidator } from '../../common/services/validation/validation.service';
import { DatabaseError, DataNotPresentInDB } from '../../common/exceptions/exceptions';
import { StoreRepository } from '../../database/StoreRepository/store.repository';
import { Test, TestingModule } from '@nestjs/testing';
import { StoreService } from './store.service';
import {
  mockedstorePostRequest,
} from './__testData__/store.testData.spec';

jest.mock('../../common/services/validation/validation.service');

jest.mock('../../database/StoreRepository/store.repository');

const getTestModules = async (): Promise<any> => {
  const metadata = {
    providers: [StoreRepository, StoreService],
  };
  const module: TestingModule = await Test.createTestingModule(metadata).compile();

  const repository = module.get<StoreRepository>(StoreRepository);
  const service = module.get<StoreService>(StoreService);
  return { repository, service };
};

describe('store post', () => {
  let service: StoreService;
  let repository: StoreRepository;

  beforeEach(async () => {
    const modules = await getTestModules();
    repository = modules.repository;
    service = modules.service;
  });

  afterEach(() => {
    jest.resetAllMocks();
    jest.clearAllMocks();
  });

  describe('Post store', () => {
    beforeEach(() => {
      jest.spyOn(RequestValidator, 'validateRequest');
      jest.spyOn(repository, 'createStore');
    });

    it('should return success response if repository returns no error', async () => {
      const response = await service.postStore(mockedstorePostRequest);

      const { storeId, storeName, addressLine1, addressLine2, country, province, region, postcode, latitude, longitude } = mockedstorePostRequest;

      expect(response).toEqual({ message: 'success' });
      expect(repository.createStore).toHaveBeenCalledWith(storeId, storeName, addressLine1, addressLine2, country, province, region, postcode, latitude, longitude);
    });

    it('should throw the same error repository throws', done => {
      jest.spyOn(repository, 'createStore').mockImplementationOnce(() => {
        throw new DatabaseError('error');
      });
      service.postStore(mockedstorePostRequest).catch(error => {
        expect(error).toBeInstanceOf(DatabaseError);
        done();
      });
    });
  });
});
