import { dbSchema } from '../../../common/config';
import { storeTable } from '../../../common/constants';

export const generateSql = (): string => {
  const { storeId, storeName, addressLine1, addressLine2, country, province, region, postcode, latitude, longitude } = storeTable;

  return `CREATE TABLE IF NOT EXISTS "${dbSchema}".stores (
      id SERIAL PRIMARY KEY,
      ${storeId} VARCHAR (255) NOT NULL,
      ${storeName} VARCHAR (255) NOT NULL,
      ${addressLine1} VARCHAR (255) NOT NULL,
      ${addressLine2} VARCHAR (255),
      ${country} VARCHAR (255) NOT NULL,
      ${province} VARCHAR (255) NOT NULL,
      ${region} VARCHAR (255) NOT NULL,
      ${postcode} VARCHAR (255) NOT NULL,
      ${latitude} VARCHAR (255) NOT NULL,
      ${longitude} VARCHAR (255) NOT NULL,
      CONSTRAINT unique_store_id UNIQUE(${storeId}));`;
};
