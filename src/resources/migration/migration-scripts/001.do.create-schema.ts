import { dbSchema } from '../../../common/config';

export const generateSql = (): string => `CREATE SCHEMA IF NOT EXISTS "${dbSchema}"`;
