import { dbSchema } from '../../../common/config';
import { storeSlotTable } from '../../../common/constants';

export const generateSql = (): string => {
  const { storeId, slotStartTime, slotEndTime, storeTimingId } = storeSlotTable;

  return `CREATE TABLE IF NOT EXISTS "${dbSchema}".store_slots (
      id SERIAL PRIMARY KEY,
      ${storeId} VARCHAR (255) NOT NULL,
      ${slotStartTime} VARCHAR (255) NOT NULL,
      ${slotEndTime} VARCHAR (255) NOT NULL,
      ${storeTimingId} INT NOT NULL,
      CONSTRAINT unique_store_id_slot_start_time_store_timing_id UNIQUE(${storeId}, ${slotStartTime}, ${storeTimingId}));`;
};
