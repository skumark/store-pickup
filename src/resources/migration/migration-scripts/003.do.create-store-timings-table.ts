import { dbSchema } from '../../../common/config';
import { storeTimingTable } from '../../../common/constants';

export const generateSql = (): string => {
  const { storeId, dayOfWeek, startTime, endTime, slotInterval, orderPerSlot, collectTime } = storeTimingTable;

  return `CREATE TABLE IF NOT EXISTS "${dbSchema}".store_timings (
      id SERIAL PRIMARY KEY,
      ${storeId} VARCHAR (255) NOT NULL,
      ${dayOfWeek} SMALLINT NOT NULL,
      ${startTime} VARCHAR (255) NOT NULL,
      ${endTime} VARCHAR (255),
      ${slotInterval} SMALLINT NOT NULL,  
      ${orderPerSlot} INT NOT NULL,
      ${collectTime} INT NOT NULL DEFAULT 10,
      CONSTRAINT unique_store_id_day UNIQUE(${storeId}, ${dayOfWeek}));`;
};
