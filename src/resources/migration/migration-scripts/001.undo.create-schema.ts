import { dbSchema } from '../../../common/config';

export const generateSql = (): string => `DELETE SCHEMA IF EXISTS "${dbSchema}"`;
