import { dbSchema } from '../../../common/config';
import { storeOrderTable } from '../../../common/constants';

export const generateSql = (): string => {
  const { orderId, userId, storeSlotId, orderTime, pickupDate } = storeOrderTable;

  return `CREATE TABLE IF NOT EXISTS "${dbSchema}".store_orders (
      id SERIAL PRIMARY KEY,
      ${orderId} VARCHAR (255) NOT NULL,
      ${userId} VARCHAR (255) NOT NULL,
      ${storeSlotId} VARCHAR (255) NOT NULL,
      ${orderTime} TIMESTAMP NOT NULL,
      ${pickupDate} DATE NOT NULL);`;
};
