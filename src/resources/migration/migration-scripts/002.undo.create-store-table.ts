import { dbSchema } from '../../../common/config';

export const generateSql = (): string => `DROP TABLE IF EXISTS "${dbSchema}".stores`;
