import * as Postgrator from 'postgrator';
import { dbConfig } from '../../common/config';
import { logError, logInfo } from '../../common/utils/io.utils';

const postgrator = new Postgrator({
  validateChecksums: true,
  migrationDirectory: __dirname + '/migration-scripts',
  driver: 'pg',
  host: dbConfig.host,
  port: dbConfig.port,
  database: dbConfig.database,
  password: dbConfig.password,
  username: dbConfig.user,
});

logInfo('DB config', { payload: dbConfig, status: '', response: {} });

postgrator
  .migrate('max')
  .then(appliedMigrations => {
    logInfo('Applied Migrations', { payload: {}, status: 'success', response: appliedMigrations });
  })
  .catch(error =>
    logError('Error in Migration', { payload: {}, status: 'error', message: error.message, stack: error.stack }),
  );
