import tracer from 'dd-trace';

/*tracer.init({
    tags: {
      app: 'sbx-messages',
      country: 'corp',
      domain: 'esti',
      capability: 'esti',
      industry: 'corp',
      sre_team: 'sre_team',
      cost_center: '123456',
      business_unit: 'architecture_corporate',
      team: 'architecture_corporate'
    }
  })*/
tracer.init(); // initialized in a different file to avoid hoisting.
export default tracer;
