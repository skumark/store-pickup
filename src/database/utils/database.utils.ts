export const convertToAny = (list: string[]): string => {
  return `{${list.map(v => JSON.stringify(v)).join(', ')}}`;
};
