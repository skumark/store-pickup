import { convertToAny } from './database.utils';

describe('Database Utils', () => {
  it('should convert the string array passed', () => {
    const convertedEntity = convertToAny(['hello', 'here']);

    expect(convertedEntity).not.toBeNull();
  });
});
