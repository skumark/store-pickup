import { StoreEntity } from './store.entity';

describe('web category mapping entity', () => {
  it('should create', () => {
    const storeEntity = new StoreEntity(
      '1',
      'FALABELLA',
      'CHILE',
      'SANTIAGO',
      '1323244',
      '9869557',
    );
    expect(storeEntity).toBeDefined();
  });
});
