import { Column, Entity, Generated, PrimaryColumn } from 'typeorm';
import { DatabaseConstants } from '../../database.constants';

@Entity(DatabaseConstants.STORE_TABLE, {
  schema: DatabaseConstants.schema,
})
export class StoreEntity {
  @Generated()
  @PrimaryColumn()
  readonly id: number;

  @Column({ name: 'store_id' })
  readonly storeId: string;

  @Column({ name: 'store_name' })
  readonly storeName: string;

  @Column({ name: 'address_line1' })
  readonly addressLine1: string;

  @Column({ name: 'address_line2' })
  readonly addressLine2: string;

  readonly latitude: string;

  readonly longitude: string;

  constructor(
    storeId: string,
    storeName: string,
    addressLine1: string,
    addressLine2: string,
    latitude: string,
    longitude: string,
  ) {
    this.storeId = storeId;
    this.storeName = storeName;
    this.addressLine1 = addressLine1;
    this.addressLine2 = addressLine2;
    this.latitude = latitude;
    this.longitude = longitude;
  }
}
