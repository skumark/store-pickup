import { Injectable } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository } from 'typeorm';
import { StoreEntity } from './entity/store.entity';
import { DatabaseConstants } from '../database.constants';
import { storeTable, storeTimingTable, storeSlotTable, storeOrderTable, httpResponse } from '../../common/constants';
import { DatabaseError } from '../../common/exceptions/exceptions';
import { logError, logInfo } from '../../common/utils/io.utils';
import { getCurrentDateTime } from '../../common/utils/helper';
import { convertToAny } from '../utils/database.utils';


@Injectable()
export class StoreRepository {
  private readonly readSuccessLog = 'DB result READ store';
  private readonly readErrorLog = 'DB error READ store';
  private readonly writeSuccessLog = 'DB result WRITE store';
  private readonly writeErrorLog = 'DB error WRITE store';

  constructor(
    @InjectRepository(StoreEntity)
    private readonly repository: Repository<StoreEntity>,
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
  ) {}

  // 1. STORE CREATE
  createStore(storeId, storeName, addressLine1, addressLine2,country, province, region, postcode, latitude, longitude): Promise<any> {
    const operation = DatabaseConstants.operations.WRITE;

    const createStoreQuery = this.createQueryStringForstoreCreate();
    const storeParams = {
      storeId,
      storeName,
      addressLine1,
      addressLine2,
      country,
      province,
      region,
      postcode,
      latitude,
      longitude,
    };

    return this.executeQuery(createStoreQuery, storeParams, operation);
  }

  // 2. STORE TIMINGS CREATE
  createTimings(storeId, dayOfWeek, startTime, endTime, slotInterval, orderPerSlot, collectTime): Promise<any> {
    const operation = DatabaseConstants.operations.WRITE;

    const createStoreTimingsQuery = this.createQueryStringForStoreTimingsCreate();
    const storeTimingsParams = {
      storeId,
      dayOfWeek, 
      startTime,
      endTime,
      slotInterval,
      orderPerSlot,
      collectTime
    };
    console.log(storeTimingsParams);
    return this.executeQuery(createStoreTimingsQuery, storeTimingsParams, operation);
  }

  // 3. STORE SLOTS CREATE
  createSlots(storeId, slotStartTime, slotEndTime, storeTimingId): Promise<any> {
    const operation = DatabaseConstants.operations.WRITE;

    const createStoreSlotsQuery = this.createQueryStringForStoreSlotsCreate();
    const storeSlotsParams = {
      storeId,
      slotStartTime,
      slotEndTime,
      storeTimingId
    };

    return this.executeQuery(createStoreSlotsQuery, storeSlotsParams, operation);
  }

  // 4. STORE ORDERS CREATE
  createOrders(orderId, userId, storeSlotId, orderTime, pickupDate): Promise<any> {
    const operation = DatabaseConstants.operations.WRITE;

    const createStoreOrdersQuery = this.createQueryStringForStoreOrdersCreate();
    const storeOrdersParams = {
      orderId,
      userId,
      storeSlotId,
      orderTime,
      pickupDate
    };


    return this.executeQuery(createStoreOrdersQuery, storeOrdersParams, operation);
  }


  // 1. STORE CREATE
  private createQueryStringForstoreCreate(): string {
    const { storeId, storeName, addressLine1, addressLine2, country, province, region, postcode, latitude, longitude } = storeTable;

    return `
    INSERT INTO ${DatabaseConstants.schema}.${DatabaseConstants.STORE_TABLE} (
    ${storeId}, ${storeName}, ${addressLine1}, ${addressLine2}, ${country}, ${province}, ${region}, ${postcode}, ${latitude}, ${longitude}
    ) VALUES(
    :storeId, :storeName, :addressLine1, :addressLine2, :country, :province, :region, :postcode, :latitude, :longitude
    ) ON CONFLICT (
    ${storeId}
    ) DO UPDATE SET 
    ${storeName} = :storeName,
    ${addressLine1} = :addressLine1,
    ${addressLine2} = :addressLine2,
    ${country} = :country,
    ${province} = :province,
    ${region} = :region,
    ${postcode} = :postcode,
    ${latitude} = :latitude,
    ${longitude} = :longitude
    `;
  }

  // 2. STORE TIMINGS CREATE
  private createQueryStringForStoreTimingsCreate(): string {
    const { storeId, dayOfWeek, startTime, endTime, slotInterval, orderPerSlot, collectTime } = storeTimingTable;

    return `
    INSERT INTO ${DatabaseConstants.schema}.${DatabaseConstants.STORE_TIMINGS_TABLE} (
    ${storeId}, ${dayOfWeek}, ${startTime}, ${endTime}, ${slotInterval}, ${orderPerSlot}, ${collectTime}
    ) VALUES(
    :storeId, :dayOfWeek, :startTime, :endTime, :slotInterval, :orderPerSlot, :collectTime
    ) ON CONFLICT (
    ${storeId}, ${dayOfWeek}
    ) DO UPDATE SET 
    ${startTime} = :startTime,
    ${endTime} = :endTime,
    ${slotInterval} = :slotInterval,
    ${orderPerSlot} = :orderPerSlot,
    ${collectTime} = :collectTime
    `;
  }

  // 3. STORE SLOTS CREATE
  private createQueryStringForStoreSlotsCreate(): string {
    const { storeId, slotStartTime, slotEndTime, storeTimingId } = storeSlotTable;

    return `
    INSERT INTO ${DatabaseConstants.schema}.${DatabaseConstants.STORE_SLOTS_TABLE} (
    ${storeId}, ${slotStartTime}, ${slotEndTime}, ${storeTimingId}
    ) VALUES(
    :storeId, :slotStartTime, :slotEndTime, :storeTimingId
    ) ON CONFLICT (
    ${storeId}, ${slotStartTime}, ${storeTimingId}
    ) DO UPDATE SET 
    ${slotStartTime} = :slotStartTime,
    ${slotEndTime} = :slotEndTime
    `;
  }

  // 4. STORE ORDERS CREATE
  private createQueryStringForStoreOrdersCreate(): string {
    const { orderId, userId, storeSlotId, orderTime, pickupDate } = storeOrderTable;

    return `
    INSERT INTO ${DatabaseConstants.schema}.${DatabaseConstants.STORE_ORDERS_TABLE} (
    ${orderId}, ${userId}, ${storeSlotId}, ${orderTime}, ${pickupDate}
    ) VALUES(
    :orderId, :userId, :storeSlotId, :orderTime, :pickupDate
    )
    `;
  }

  //1. GET STORE DETAILS
  async getStoreDetails(storeId: string): Promise<any> {
    const operation = DatabaseConstants.operations.READ;
    const queryString = this.createQueryStringForReadStoreDetails();
    const params = { storeId };
    const storeDetails = await this.executeQuery<object>(queryString, params, operation);

    return storeDetails;
  }

  //2. GET STORE AVAILABILITY DETAILS
  async getStoreAvailability(storeId: string, date: string): Promise<any> {
    const operation = DatabaseConstants.operations.READ;
    const queryString = this.createQueryStringForReadStoreAvailabilityDetails();
    const params = { storeId, date };
    const storeAvailabilityDetails = await this.executeQuery<object>(queryString, params, operation);

    return storeAvailabilityDetails;
  }

  //3. GET ALL STORE DETAILS
  async getAllStores(country: string, province: string): Promise<any> {
    const operation = DatabaseConstants.operations.READ;
    const queryString = this.createQueryStringForReadAllStoreDetails();
    const params = { country, province };
    const allStoreDetails = await this.executeQuery<object>(queryString, params, operation);

    return allStoreDetails;
  }

  //1. GET STORE DETAILS
  private createQueryStringForReadStoreDetails(): string {
    const { storeId } = storeTable;
    return `
     SELECT * FROM ${DatabaseConstants.schema}.${DatabaseConstants.STORE_TABLE} 
     WHERE ${storeId} = :storeId
     `;
  }

  //2. GET STORE AVAILABILITY DETAILS
  private createQueryStringForReadStoreAvailabilityDetails(): string {
    const { storeId, dayOfWeek, startTime, endTime, slotInterval, orderPerSlot, collectTime } = storeTimingTable;
    return `
     SELECT * FROM ${DatabaseConstants.schema}.${DatabaseConstants.STORE_TIMINGS_TABLE} 
     WHERE ${storeId} = :storeId
     `;
  }

  //3. GET ALL STORE DETAILS
  private createQueryStringForReadAllStoreDetails(): string {
    const { storeId } = storeTable;
    return `
     SELECT * FROM ${DatabaseConstants.schema}.${DatabaseConstants.STORE_TABLE}
     `;
  }

  private async executeQuery<T>(queryString: string, params, operation): Promise<T> {
    const driver = this.entityManager.connection.driver;
    const [query, parameters] = driver.escapeQueryWithParameters(queryString, params, {});
    try {
      const result = await this.entityManager.query(query, parameters);
      this.logSuccess(params, result, operation);
      return result;
    } catch (error) {
      this.logFailure(params, error, operation);
      throw new DatabaseError(error.message);
    }
  }

  private logSuccess(payloadParams, result, operation: string): void {
    const message = this.getSuccessLog(operation);
    const payload = { ...payloadParams };
    const status = httpResponse.SUCCESS;
    logInfo(message, { payload, status, response: result });
  }

  private logFailure(payloadParams, error, operation: string): void {
    const message = this.getErrorLog(operation);
    const payload = { ...payloadParams };
    const status = httpResponse.ERROR;
    logError(message, { payload, status, response: error.stack });
  }

  private getSuccessLog(operation: string): string {
    const { WRITE, READ } = DatabaseConstants.operations;
    switch (operation) {
      case READ:
        return this.readSuccessLog;
      case WRITE:
        return this.writeSuccessLog;
      default:
        return null;
    }
  }

  private getErrorLog(operation: string): string {
    const { WRITE, READ } = DatabaseConstants.operations;
    switch (operation) {
      case READ:
        return this.readErrorLog;
      case WRITE:
        return this.writeErrorLog;
      default:
        return null;
    }
  }
}
