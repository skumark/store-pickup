import { Module } from '@nestjs/common';
import typeOrmConfig from './typeOrm.config';

import { TypeOrmModule } from '@nestjs/typeorm';

import { StoreRepository } from './storeRepository/store.repository';
import { StoreEntity } from './storeRepository/entity/store.entity';

@Module({
  imports: [typeOrmConfig(), TypeOrmModule.forFeature([StoreEntity])],
  providers: [StoreRepository],
  exports: [StoreRepository],
})
export class DatabaseModule {}
