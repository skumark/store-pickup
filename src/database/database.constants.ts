export class DatabaseConstants {
  private static SCHEMA_NAME = 'store_pickup';
  public static readonly operations = { READ: 'READ', WRITE: 'WRITE', DELETE: 'DELETE' };
  public static readonly STORE_TABLE = 'stores';
  public static readonly STORE_TIMINGS_TABLE = 'store_timings';
  public static readonly STORE_SLOTS_TABLE = 'store_slots';
  public static readonly STORE_ORDERS_TABLE = 'store_orders';

  static get schema(): string {
    return this.SCHEMA_NAME;
  }
}
