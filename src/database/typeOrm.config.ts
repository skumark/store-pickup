import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ConnectionOptions } from 'typeorm';

const getConfigValues = (configService: ConfigService): any => {
  const host: string = configService.get('DB_HOST');
  const port: number = +configService.get('DB_PORT');
  const username: string = configService.get('DB_USER');
  const password: string = configService.get('DB_PASSWORD');
  const database: string = configService.get('DB_NAME');
  return { host, port, username, password, database };
};

const getDBConfiguration = (configService: ConfigService): ConnectionOptions => {
  const configValues = getConfigValues(configService);
  return {
    type: 'postgres',
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
    synchronize: false,
    autoLoadEntities: true,
    migrationsRun: true,
    ...configValues,
  };
};

export default function typeOrmConfig(): any {
  return TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    useFactory: getDBConfiguration,
    inject: [ConfigService],
  });
}
