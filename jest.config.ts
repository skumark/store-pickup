module.exports = {
  modulePathIgnorePatterns: ['__testData__', 'dto'],
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: 'src',
  testRegex: '.spec.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  collectCoverage: true,
  coverageReporters: ['html', 'text'],
  coverageThreshold: {
    global: {
      statements: 50,
      branches: 50,
      functions: 50,
      lines: 50,
    },
  },
  coverageDirectory: '../coverage',
  testEnvironment: 'node',
  clearMocks: true,
};
